"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Modal = void 0;
// создать в разделе models интерфейс для класса и  применить реализацию к этому классу
var Modal = /** @class */ (function () {
    function Modal(id) {
        if (id === void 0) { id = null; }
        var _this = this;
        this.closeModalFunction = function (ev) {
            var targetItem = ev.target;
            if (targetItem.classList.contains('close-modal')) {
                _this.remove();
            }
        };
        var findModal = Modal.modals.find(function (x) { return x.id === id; });
        if (findModal) {
            Modal.removeById(id);
        }
        Modal.modals.push(this);
        console.log("Modal.modals", Modal.modals);
        this.id = id || (Math.random() + Modal.modals.length).toString();
    }
    Modal.prototype.open = function (template) {
        var divWrap = document.createElement("div");
        divWrap.innerHTML = template;
        divWrap.id = this.id;
        divWrap.setAttribute("modal-id", this.id);
        divWrap.classList.add("modal_element");
        divWrap.addEventListener('click', this.closeModalFunction);
        document.body.appendChild(divWrap);
    };
    Modal.prototype.remove = function () {
        var modalEl = document.getElementById(this.id);
        if (modalEl) {
            modalEl.removeEventListener('click', this.closeModalFunction);
            modalEl.parentNode.removeChild(modalEl);
        }
    };
    Modal.removeById = function (id) {
        if (id === void 0) { id = null; }
        var modalId = id;
        var findEl = Modal.modals.find(function (x) { return x.id === modalId; });
        if (findEl) {
            findEl.remove();
            Modal.modals = Modal.modals.filter(function (el) { return el.id !== modalId; });
        }
        else {
            if (Array.isArray(Modal.modals)) {
                var lastEl = Modal.modals.pop();
                if (lastEl) {
                    lastEl.remove();
                }
            }
        }
    };
    Modal.modals = [];
    return Modal;
}());
exports.Modal = Modal;
//# sourceMappingURL=modal.js.map