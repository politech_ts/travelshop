
// создать в разделе models интерфейс для класса и  применить реализацию к этому классу
export class Modal  {
  // здесь должны быть поля и методы класса
    private readonly id: string;
    private closeModalFunction = (ev:MouseEvent) => {
        const targetItem = ev.target as HTMLElement;
        if (targetItem.classList.contains('close-modal')){
            this.remove();
        }
    }
    public static modals: any[] = [];

    constructor(id = null) {

        const findModal = Modal.modals.find(x => x.id === id);
        if (findModal) {
            Modal.removeById(id);
        }

        Modal.modals.push(this);
        console.log("Modal.modals", Modal.modals);
        this.id = id || (Math.random() + Modal.modals.length).toString();
    }
    public open (template: string):void {
        const  divWrap = document.createElement("div");
        divWrap.innerHTML = template;
        divWrap.id = this.id;
        divWrap.setAttribute("modal-id", this.id);
        divWrap.classList.add("modal_element");
        divWrap.addEventListener('click',this.closeModalFunction);
        document.body.appendChild(divWrap)
    }
    public remove():void{
        const modalEl = document.getElementById(this.id);
        if (modalEl) {
            modalEl.removeEventListener('click', this.closeModalFunction)

            modalEl.parentNode.removeChild(modalEl);
        }
    }
    public static removeById(id = null):void{
        let modalId = id;

        const findEl = Modal.modals.find(x => x.id === modalId);
        if (findEl) {
            findEl.remove();
            Modal.modals = Modal.modals.filter((el) => el.id !== modalId)
        }  else {
            if (Array.isArray(Modal.modals)){
                const lastEl = Modal.modals.pop();
                if (lastEl){
                    lastEl.remove();
                }
            }
        }
    }
}