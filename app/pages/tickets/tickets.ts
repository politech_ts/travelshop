import {getTicketById, postTicketData} from "@rest/tickets";
import '@myCss'; // добавлена новая ссылка - ссылка ведет на один файл
import '@assets/styles/main.scss';
import '@assets/styles/tickets.scss'
import {initTicketElementTemplate} from "../../templates/ticketInfo";
import {IVipTicket, TicketType, ITicket} from "../../models/ticket/ticket";
import {initFooterTitle, initHeaderTitle} from "@services/general/general";
import {IUser} from "../../models/user/user";


//let ticketInstance: TicketType ;
//let ticketPostInstance: string;
const clientType = "custom";


// init main  data
initApp();
registerConfirmButton();



function initApp(): void {
    console.log(`initApp`)
    const ticketData: Promise<IVipTicket[]> = getTicketById<IVipTicket>('someId');
    ticketData.then((data): void => {
        let ticketInstance: TicketType  = data[0];
        const ticketName:string = typeof ticketInstance?.name === "string" ? ticketInstance?.name : '';
        initHeaderTitle(ticketName, 'h2');
        initFooterTitle('Туры по всему миру', 'h3');
        initTicketInfo(ticketInstance);
    });
}

/*  - перенести все методы ниже в раздел services (сюда импортировать и вызывать)
    - Указать в методах возвращающие типы, в теле функции также указать типы чтобы не было ошибок
*/

function initTicketInfo(ticket: TicketType) {
    const targetElement:Element = document.querySelector('.ticket-info');

    const ticketDescription:string = ticket?.description;
    const ticketOperator:string = ticket?.tourOperator;
    const vipClientType:string = (ticket as IVipTicket).vipStatus;

    const ticketElemsArr: [string, string, string] = [ticketDescription, ticketOperator, vipClientType];

    let ticketElemTemplate:string;

    ticketElemsArr.forEach((el, i) => {
        ticketElemTemplate+= initTicketElementTemplate(el, i);
    });

    targetElement.innerHTML = ticketElemTemplate;

}

function initUserData():IUser {
const userInfo: NodeListOf<Element> = document.querySelectorAll('.user-info > p');
let userInfoObj : IUser
    userInfo.forEach((el) => {
    const inputDataName:string = el.getAttribute('data-name');
    if (inputDataName) {
        const inputElems:HTMLInputElement = el.querySelector('input');
        userInfoObj[inputDataName] = inputElems.value;
    }
    });

    console.log('userInfoObj',userInfoObj)
    return userInfoObj;
}

function initPostData(data: string) :void{
    initUserData();
    postTicketData(data).then((data) => {
        if (data.success) {

        }
    })
}

function registerConfirmButton(): void {
    let ticketPostInstance: string;
    const targetEl:Element = document.getElementById('accept-order-button');
    if (targetEl) {
        targetEl.addEventListener('click', () => {
            initPostData(ticketPostInstance);
        });
    }
}


