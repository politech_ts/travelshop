// определить интерфейс IUser

export interface IUser {
    name: string,
    cardNumber: number,
    birthDate: string
}